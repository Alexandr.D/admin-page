/**
 * Created by User on 13.09.2018.
 */
({
    // Init table labels custom objects
    init : function(component, event, helper) {
        console.log('helper');
        helper.helperInit(component, event, helper);
    },

    // Enable live search
    liveSearchController : function (component, event, helper) {
        helper.liveSearchHelper(component, event, helper);
    },

    // Method for view parents, child, edit style of changed row
    checkSObjectController : function (component, event, helper) {
         helper.checkParentHelper(component, event, helper);
         helper.checkChildHelper(component, event, helper);
         helper.changeStyle(component, event, helper);
    },

    // Open column for viewing parents, child
    openModel : function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
    },

    // Close column parents, child
    closeModel : function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"
        component.set("v.isOpen", false);
    },

    // Open popup for deleting records
    openPopup : function(component, event, helper) {
        // for Display Popup, set the "isOpen" attribute to "true"
        helper.openPopupHelper(component, event, helper);

    }

})