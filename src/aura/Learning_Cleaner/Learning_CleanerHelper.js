/**
 * Created by User on 13.09.2018.
 */
({
    helperInit : function (component, event, helper) {

        var action = component.get("c.getAllCustomObjects");
        action.setCallback(this, function(response) {

           var state = response.getState();

            if (state === 'SUCCESS') {
                var testObject  = response.getReturnValue();
                var custs       = [];
                var conts       = response.getReturnValue();

                //Here we are creating the list to show on UI.
                for(var key in conts){
                     custs.push({value:conts[key], key:key});
                }

                component.set('v.dataForView', custs);

            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);

    },

    liveSearchHelper : function myFunction(component, event, helper) {
         var tr, trObj, inputText, tbodyTable, tableId, filter, td;
         inputText = component.get("v.searchText").toUpperCase();
         tableId = document.getElementById("myTable");
         tbodyTable = document.getElementById("tbody");
         tr = tbodyTable.getElementsByTagName("tr");


         for (let i = 0; i < tr.length; i++) {
             td = tr[i].getElementsByTagName("td")[0];
             if (td) {
             console.log(td);
                 if (td.innerHTML.toUpperCase().indexOf(inputText) > -1) {
                     tr[i].style.display = "";
                 } else {
                     tr[i].style.display = "none";
                 }
             }
         }
    },

    changeStyle : function(component,event,helper){


         var cmp            = event.currentTarget;
         var numberOfRow    = cmp.dataset.index;
         var apiName        = cmp.dataset.idshka;
         var button         = component.find('button');
         var rowOfTable     = component.find('tr');

         for( cmp in rowOfTable) {

             $A.util.removeClass(rowOfTable[cmp], 'toggle');
             $A.util.removeClass(button[cmp], 'slds-visible');
             $A.util.addClass(button[cmp], 'slds-hidden');

         } //end for

         $A.util.toggleClass(rowOfTable[numberOfRow], 'toggle');
         $A.util.removeClass(button[numberOfRow], 'slds-hidden');
         $A.util.addClass(button[numberOfRow], 'slds-visible');

         console.log('row NOW   ' + rowOfTable[numberOfRow]);

    },

    checkParentHelper : function(component,event,helper) {
         var cmp            = event.currentTarget;
         var numberOfRow    = cmp.dataset.index;
         var apiName        = cmp.dataset.idshka;
         var apiNameList    = [];
         apiNameList.push(apiName);

         var actionParent = component.get('c.getParentsOfSelectedObject');
             actionParent.setParams({ "listIncomingObjects" : apiNameList });
             actionParent.setCallback(this, $A.getCallback(function (response) {
                console.log('RESPONSE: '+response);

                 var state = response.getState();
                    if(state === "SUCCESS") {
                        var parents = [];
                        var parentsResponse = response.getReturnValue();
                        console.log('map in enter' + JSON.stringify(parentsResponse));

                        //Here we are creating the list to show on UI.
                        for (var key in parentsResponse){

                            parents.push({value:parentsResponse[key], key:key});

                        }
                    component.set('v.listParents', parents);

                    } else if (state === "ERROR") {
                        var errors = response.getError();
                        console.error(errors);
                    }
             }));
         $A.enqueueAction(actionParent);

    },


    checkChildHelper : function(component,event,helper) {
        var cmp             = event.currentTarget;
        var numberOfRow     = cmp.dataset.index;
        var apiName         = cmp.dataset.idshka;
        var apiNameForChild = [];
        var clearMap        = ([]);
        var actionParent    = component.get('c.getChildrenOfSelectedObject');
        var levelHelper     = component.get("v.level");
        apiNameForChild.push(apiName);

        actionParent.setParams({ "incomingObjects" : apiNameForChild,
                                  level : levelHelper
                              });

        actionParent.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();

            if(state === "SUCCESS") {
                var children          = [];
                var childrenResponse  = response.getReturnValue();

                for(var key in childrenResponse){
                    //Here we are creating the list to show on UI.
                    children.push({value : childrenResponse[key], key:key});

                } //end for
                delete children[0];
                component.set('v.listChilds', children);

            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            } //end if
        })); //end setCallback
        $A.enqueueAction(actionParent);
    },

    openPopupHelper : function(component,event,helper) {
        component.set("v.isOpenPopup", true);
        var ApiNameOnRow = event.getSource().get("v.tabindex");
        console.log(">>>>ApiNameOnRow" + ApiNameOnRow);
        component.set("v.ApiNameToDeleteParent", ApiNameOnRow);
    }

})