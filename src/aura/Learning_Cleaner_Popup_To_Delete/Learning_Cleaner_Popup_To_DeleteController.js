/**
 * Created by User on 24.09.2018.
 */
({
    deleteChangedRecords : function(component, event, helper) {
        helper.deleteChangedRecordsHelper(component, event, helper);
        component.set("v.isOpenPopupChild", false);
    },

        // Close popup for deleting records
    closePopup : function(component, event, helper) {
        component.set("v.isOpenPopupChild", false);
    },
        // for deleting Learning Objects
    changeCheckBox : function(component, event, helper) {

        component.set("v.checkBox", true);
    }
})