/**
 * Created by User on 24.09.2018.
 */
({
    deleteChangedRecordsHelper : function(component,event,helper) {

        var checkBoxRow;
        var apiName = event.getSource().get("v.tabindex");
        var checkBox = component.find("checkBox").get("v.checked");
        var apiNameForDelete = [];
        var actionDelete = component.get('c.deleteRecordsOfObjects');
        apiNameForDelete.push(apiName);

        if(checkBox === true) {
            checkBoxRow = true;
        } else {
            checkBoxRow = false;
        }

        actionDelete.setParams({ "incomingObjects" : apiNameForDelete,
                                                     "checkbox" : checkBoxRow });

        $A.enqueueAction(actionDelete);

    }
})