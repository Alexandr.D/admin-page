public with sharing class PageToAssignController {

    public String               incomingCohort              { get; set; }
    public String               incomingOrganization        { get; set; }
    public String               incomingSeGroup             { get; set; }
    public String               incomingLearningTrac        { get; set; }
    public List<User>           allUsersSet                 { get; set; }
    public List<User>           allUsersSeGroupSet          { get; set; }
    public List<User>           listUsersToAssign           { get; set; }
    public String               messageAlreadyToAssign      { get; set; }
    public Boolean              checkBox                    { get; set; }
    public List<String>         allUserSetIds               { get; set; }
    public String               trackValue                  { get; set; }
    public String               testId                      { get; set; }
    public String               withWindow                  { get; set; }



    public PageToAssignController(){
        checkBox = false;
        testId = 'no ';
        listUsersToAssign       = new List<User>();
        allUsersSet             = new List<User>();
        withWindow = 'sizeBrowser();';
        //allUserSetIds           = new List<String>();
    }


//    PICKLISTS
    public List<SelectOption> getCohortName () {
        List<SelectOption> listCohorts = new List<SelectOption>();
        listCohorts.add(new SelectOption('', '- Change Cohort -'));
        for(Group oneGroup : [SELECT DeveloperName
                              FROM Group
                              WHERE Type = 'Regular']) {
            listCohorts.add(new SelectOption(oneGroup.DeveloperName, oneGroup.DeveloperName));
        }
        return listCohorts;
    }

    public List<SelectOption> getOrganizationsName () {
        List<SelectOption> listOrganizations = new List<SelectOption>();
        listOrganizations.add(new SelectOption('', '- Change Organization -'));
        for(Organization__c oneOrganization : [SELECT Name
                                               FROM Organization__c]) {
            listOrganizations.add(new SelectOption(oneOrganization.Name, oneOrganization.Name));
        }
        return listOrganizations;
    }

    public List<SelectOption> getSegroupsName () {

        Set<SelectOption> setSeGroups = new Set<SelectOption>();
        setSeGroups.add(new SelectOption('', '- Change SE Group -'));
        for(CollaborationGroupMember oneGroupMember :  [SELECT CollaborationGroupId, MemberId, CollaborationGroup.Name
                                                        FROM CollaborationGroupMember
                                                        WHERE MemberId IN :allUserSetIds]) {

            setSeGroups.add(new SelectOption(oneGroupMember.CollaborationGroup.Name, oneGroupMember.CollaborationGroup.Name));
        }
        List<SelectOption> listSeGroups = new List<SelectOption>(setSeGroups);
        return listSeGroups;
    }

    public List<SelectOption> getTracksName () {
        List<SelectOption> listTracs = new List<SelectOption>();
        listTracs.add(new SelectOption('', '- Change Track -'));
        for(redwing__Training_Track__c oneTrack :  [SELECT Name
                                                    FROM redwing__Training_Track__c]) {
            listTracs.add(new SelectOption(oneTrack.Id, oneTrack.Name));
        }
        return listTracs;
    }

    public void UsersFromCohort() {
        //for viewing all users in cohort
        allUserSetIds           = new List<String>();
        Set<String> setUsersInCohortId = new Set<String>();
        for(GroupMember groupMemberInLoop : [SELECT Id, UserOrGroupId, GroupId
                                             FROM GroupMember
                                             WHERE Group.Name = :incomingCohort]) {
            setUsersInCohortId.add(groupMemberInLoop.UserOrGroupId);
            allUserSetIds.add(groupMemberInLoop.UserOrGroupId);
        }

        Set<String> setOrganizations = new Set<String>();
        for(Organization__c organizationInLoop : [SELECT Id
                                                  FROM Organization__c
                                                  WHERE Name = :incomingOrganization]) {
            setOrganizations.add(organizationInLoop.Id);
        }

        Set<String> usersInJunctionSet = new Set<String>();
        for(Junction_Object__c junctionInLoop : [SELECT FromJunctionObject__c
                                                 FROM Junction_Object__c
                                                 WHERE ChildOrganization__c  IN :setOrganizations]) {
            usersInJunctionSet.add(junctionInLoop.FromJunctionObject__c);
        }

        if(incomingOrganization == null) {
            allUsersSet.clear();
            for(User userInLoop : [SELECT Name, Mentor__c,Active_mentor__c
                                   FROM User
                                   WHERE Id IN :setUsersInCohortId]) {
                allUsersSet.add(userInLoop);

            }
            //for viewing users of changed organization in cohort
        } else {
            allUsersSet.clear();
            for(User userInLoop : [SELECT Name, Mentor__c,Active_mentor__c
                                   FROM User
                                   WHERE Id IN :usersInJunctionSet
                                   AND Id IN :setUsersInCohortId]) {
                allUsersSet.add(userInLoop);
            }
        }

    }

    public void UsersFromSeGroup() {
        Set<String> setUser = new Set<String>();
        for(CollaborationGroupMember gm  :  [SELECT Id, MemberId, CollaborationGroup.Name
                                             FROM CollaborationGroupMember
                                             WHERE CollaborationGroup.Name = :incomingSeGroup]) {
            setUser.add(gm.MemberId);
        }
        allUsersSeGroupSet = new List<User>();
        for(User userInLoop  : [SELECT Name, Mentor__c,Active_mentor__c
                                FROM User
                                WHERE Id IN :setUser
                                AND Mentor__c = false]) {
            allUsersSeGroupSet.add(userInLoop);
        }
    }

    public void changeTrack (){
        trackValue = ApexPages.CurrentPage().getParameters().get('valueTrack');
    }


    public void AddUsersToAssign() {
        String addUserNameToAssign = ApexPages.CurrentPage().getParameters().get('IdAdd');
        for(User userToList : allUsersSet) {
            //stop duplicate in list
            if(listUsersToAssign.contains(userToList) && userToList.Id == addUserNameToAssign) {
                // nothing to do
                messageAlreadyToAssign = 'User ' + userToList.Name + ' is already to assign.';
            } else if (userToList.Id == addUserNameToAssign) {
                listUsersToAssign.add(userToList);
                messageAlreadyToAssign = '';
            }
        }
    }

    public void RemoveUsersToAssign() {

        String removeUserNameToAssign = ApexPages.CurrentPage().getParameters().get('IdRemove');
        Integer intList;
        for(User userToList : listUsersToAssign) {
            if (userToList.Id == removeUserNameToAssign) {
                intList = (listUsersToAssign.indexOf(userToList));
            }
        }
        listUsersToAssign.remove(intList);
    }

    public void AddMentorForUserToAssign() {
        String UserNameToCheckMentor = ApexPages.CurrentPage().getParameters().get('IdMentor');
        for(Integer i =0; i<listUsersToAssign.size();i++) {
            if (listUsersToAssign[i].Id == UserNameToCheckMentor) {
                if(i !=(listUsersToAssign.size()-1)){
                    if(listUsersToAssign[i].Active_mentor__c == false){
                        listUsersToAssign[i].Active_mentor__c= true;
                    } else {
                        listUsersToAssign[i].Active_mentor__c = false;
                    }
                }
            }
        }
    }

    public void ClearListToAssign() {
        listUsersToAssign       = new List<User>();
        messageAlreadyToAssign = '';
    }

    public void assignTrack (){
        update listUsersToAssign;
    }
}