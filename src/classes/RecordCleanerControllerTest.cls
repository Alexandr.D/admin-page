@isTest
public with sharing class RecordCleanerControllerTest {

    @isTest
    public static void testAllObjects() {

        Map<String, Schema.SObjectType> mapGlobalDescribe = Schema.getGlobalDescribe();
        Map<String, String> testMap = new Map<String, String>();

        for (String item : mapGlobalDescribe.keySet()) {
            if ((item.containsIgnoreCase('achievement') ||
                 item.containsIgnoreCase('learning') ||
                 item.containsIgnoreCase('training')) &&
                 item.containsIgnoreCase('__c')) {
                 String incomingItem = item.substringBefore('__');
                 String shortItem = item.substringBeforeLast('__c').substringAfter('__');
                 List<String> elements = shortItem.split('_');
                 String result = incomingItem + '__';
                 for (String count : elements) {
                     result += count.substring(0, 1).toUpperCase() + count.substring(1, count.length()) + '_';
                 }
                 result += '_c';
                 testMap.put(result, mapGlobalDescribe.get(item).getDescribe().getLabel());
            }
        }
        System.assertEquals(testMap, RecordCleanerController.getAllCustomObjects());
    }

    @isTest
    public static void testParentObjects() {

        List<String> testListMap = new List<String>{'redwing__Training_Plan_Section__c'};
        Map<Decimal, List<String>> testMap = new Map<Decimal, List<String>>();
        List<String> testList = new List<String>{'redwing__Training_Plan__c'};
        testMap.put(0, testList);
        System.assertEquals(testMap, RecordCleanerController.getParentsOfSelectedObject(testListMap));
    }

    @isTest
    public static void testChildObjects() {

        Map<Decimal, List<String>> testMap = new Map<Decimal, List<String>>();
        List<String> testList = new List<String>{'redwing__Learning_Assignment__c'};
        List<String> testListChildren = new List<String>{'redwing__Shared_Learning_Assignment__c'};
        testMap.put(0, testList);
        testMap.put(1, testListChildren);
        System.assertEquals(testMap, RecordCleanerController.getChildrenOfSelectedObject(testList, 0));
    }

    @isTest
    public static void testCatchParentsObjects() {

        Map<Decimal, List<String>> testMap = new Map<Decimal, List<String>>();
        List<String> testList = new List<String>();
        testMap.put(0, testList);
        System.assertEquals(testMap, RecordCleanerController.getParentsOfSelectedObject(null));
    }

    @isTest
    public static void testCatchChildrenObjects() {

        Map<Decimal, List<String>> testMap = new Map<Decimal, List<String>>();
        System.assertEquals(testMap, RecordCleanerController.getChildrenOfSelectedObject(null, null));
    }

    @isTest
    public static void testCatchDeleteObjects() {

        List<redwing__Training_Plan__c> listEquals = new List<redwing__Training_Plan__c>();
        RecordCleanerController.deleteRecordsOfObjects(null, null);
        List<redwing__Training_Plan__c> testSOQL = [SELECT Id FROM redwing__Training_Plan__c];
        System.assertEquals(listEquals, testSOQL);
    }

    @isTest
    public static void testDeleteObjects() {

        List<redwing__Training_Plan_Section_Item__c> listEquals = new List<redwing__Training_Plan_Section_Item__c>();
        List<String> testList = new List<String>{'redwing__Training_Plan_Section_Item__c'};
        RecordCleanerController.deleteRecordsOfObjects(testList, true);
        List<redwing__Training_Plan_Section_Item__c> testSOQL = [SELECT Id FROM redwing__Training_Plan_Section_Item__c];
        System.assertEquals(listEquals, testSOQL);
    }
}