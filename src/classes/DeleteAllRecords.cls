global class DeleteAllRecords implements Queueable {

	private Map<Decimal, List<String>> currentObjectsLevel = new Map<Decimal, List<String>>();
	private List<SObject> recordsToDelete = new List<SObject>();

	global DeleteAllRecords(Map<Decimal, List<String>> currentObjectsLevel){
		this.currentObjectsLevel = currentObjectsLevel;
	}

	global void execute(QueueableContext context) {
		if (!currentObjectsLevel.isEmpty()) {
			if (currentObjectsLevel.get((currentObjectsLevel.size() - 1)).size() != null){
				for (String item : currentObjectsLevel.get((currentObjectsLevel.size() - 1))) {
					recordsToDelete.addAll(Database.query('SELECT Id FROM ' + item + ' '));
				}
				try {
					delete recordsToDelete;
				} catch(dmlexception e) {}
				currentObjectsLevel.remove((currentObjectsLevel.size() - 1));
				try {
					System.enqueueJob(new DeleteAllRecords(currentObjectsLevel));
				} catch (AsyncException e){}
			}
		}
	}
}