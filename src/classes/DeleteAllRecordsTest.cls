@isTest
public with sharing class DeleteAllRecordsTest {

	@isTest	static void testQueueable() {
		Map<Decimal,List<String>> currentObjectLevels = new Map<Decimal,List<String>>();
		List<String>firstTestListStrings = new List<String>();
		List<String>secondTestListStrings = new List<String>();
		List<String>thirdTestListStrings = new List<String>();
		firstTestListStrings.add('redwing__Training_Activity__c');
		secondTestListStrings.add('redwing__Training_Plan__c');
		thirdTestListStrings.add('redwing__Training_Plan_Section__c');
		currentObjectLevels.put(1,thirdTestListStrings);
		currentObjectLevels.put(2,thirdTestListStrings);
		currentObjectLevels.put(3,firstTestListStrings);
		DeleteAllRecords testDeleteAllRecords = new DeleteAllRecords(currentObjectLevels);
		System.enqueueJob(testDeleteAllRecords);
	}

	@isTest static void testQueueableException(){
		Map<Decimal,List<String>> currentObjectLevels = new Map<Decimal,List<String>>();
		DeleteAllRecords testDeleteAllRecords = new DeleteAllRecords(currentObjectLevels);
		System.assert(true);
	}
}