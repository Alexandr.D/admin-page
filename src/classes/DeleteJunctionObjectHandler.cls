public with sharing class DeleteJunctionObjectHandler {
    public void deleteJunction  (List<Junction_Object__c> listJunctionObjects){
        List<String> listId = new List<String>();
        for(Junction_Object__c j: listJunctionObjects ){
            listId.add(j.FromJunctionObject__c);
        }
        List<CollaborationGroupMember> listCollaborationGroupMember = [SELECT MemberId FROM CollaborationGroupMember WHERE MemberId =: listId];
        delete listCollaborationGroupMember;

    }
}