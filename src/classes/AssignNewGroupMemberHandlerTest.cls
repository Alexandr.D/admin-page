@isTest
public with sharing class AssignNewGroupMemberHandlerTest {
    @testSetup static void setup() {
        CollaborationGroup grp = new CollaborationGroup();
        grp.name = 'TestGroup';
        grp.CollaborationType = 'Unlisted';
        Insert grp;

        User usr = TestUser();
        User usrMentor = TestUserMentor();
        List<CollaborationGroupMember> grpMem1 = new List<CollaborationGroupMember>();
        grpMem1.add(new CollaborationGroupMember(MemberId = usr.Id, CollaborationGroupId = grp.Id));
        grpMem1.add(new CollaborationGroupMember(MemberId = usrMentor.Id, CollaborationGroupId = grp.Id));
        Insert grpMem1;

        List<redwing__Learning_Assignment__c> listLearningAssignments = new List<redwing__Learning_Assignment__c>();
        listLearningAssignments.add(new redwing__Learning_Assignment__c(redwing__Progress__c = 'Completed',Multiple_Completion__c = true,Learning_group__c = '',
                Mentor_1_look__c = null, Mentor_2_look__c = usrMentor.id, Mentor_3_look__c = usrMentor.id, redwing__User__c = usr.Id, redwing__Progress_Percentage__c = 1.10));
        listLearningAssignments.add(new redwing__Learning_Assignment__c(redwing__Progress__c = 'Completed',Multiple_Completion__c = true,Learning_group__c = '',
                Mentor_1_look__c = usrMentor.id, Mentor_2_look__c = null, Mentor_3_look__c = usrMentor.id, redwing__User__c = usr.Id, redwing__Progress_Percentage__c = 1.10));
        listLearningAssignments.add(new redwing__Learning_Assignment__c(redwing__Progress__c = 'Completed',Multiple_Completion__c = true,Learning_group__c = '',
                Mentor_1_look__c = usrMentor.id, Mentor_2_look__c = usrMentor.id, Mentor_3_look__c = null, redwing__User__c = usr.Id, redwing__Progress_Percentage__c = 1.10));
        insert listLearningAssignments;
    }

    @isTest
    public static void assignNewMemberTest(){
        List<CollaborationGroupMember> listCollaborationGroupMembers = [SELECT MemberId,CollaborationGroupId FROM CollaborationGroupMember];
        AssignNewGroupMemberHandler angmh = new AssignNewGroupMemberHandler();
        angmh.assignNewMember(listCollaborationGroupMembers);
        List<redwing__Learning_Assignment__c> listLearningAssignments = [SELECT Mentor_1_look__c FROM redwing__Learning_Assignment__c];

        System.assertEquals(listLearningAssignments[0].Mentor_1_look__c, listCollaborationGroupMembers[1].MemberId);

    }
    @isTest
    public static void deleteLearningGroupTest(){
        List<CollaborationGroupMember> listCollaborationGroupMembers = [SELECT MemberId,CollaborationGroupId FROM CollaborationGroupMember];
        AssignNewGroupMemberHandler angmh = new AssignNewGroupMemberHandler();
        angmh.deleteLearningGroup(listCollaborationGroupMembers);
        List<redwing__Learning_Assignment__c> listLearningAssignments = [SELECT redwing__Progress__c FROM redwing__Learning_Assignment__c];
        System.assertEquals(listLearningAssignments[0].redwing__Progress__c, 'Not Started');
    }

    private static User TestUser() {
        Profile p = [select Id, Name from Profile where Name = 'Standard User'];
        User u = new User(
                UserName = 'tst-user@test-company.com',
                FirstName = 'Tst-First-Name',
                LastName = 'Test-Last-Name',
                Alias = 'test',
                Email = 'tesst-user@test-company.com',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocalesIdKey = 'en_US',
                TimezonesIdKey = 'America/Los_Angeles',
                ProfileId = p.Id,
                Active_mentor__c = true
        );
        insert u;
        return u;
    }
    private static User TestUserMentor() {
        Profile p = [select Id, Name from Profile where Name = 'Standard User'];
        User u = new User(
                UserName = 'tstuser@test-company.com',
                FirstName = 'TstFirstName',
                LastName = 'Tst-Last-Name',
                Alias = 'test',
                Email = 'tesst-user@test-company.com',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocalesIdKey = 'en_US',
                TimezonesIdKey = 'America/Los_Angeles',
                ProfileId = p.Id,
                Active_mentor__c = true
        );
        insert u;
        return u;
    }

}