public class UpdateGroupMembersHandler {

    public void assignCompletedTask(List<redwing__Learning_Assignment__c> updatedProgress) {
        List<redwing__Learning_Assignment__c> listLA = getAllLAWithMultipleCompletion();
        Boolean change = false;
        for (redwing__Learning_Assignment__c newLAitem : updatedProgress) {
            for (redwing__Learning_Assignment__c oldLAitem : listLA) {
                if (newLAitem.redwing__Training_Plan__c       == oldLAitem.redwing__Training_Plan__c &&
                    newLAitem.redwing__Learning__c            == oldLAitem.redwing__Learning__c &&
                    newLAitem.redwing__Progress__c            != oldLAitem.redwing__Progress__c &&
                    newLAitem.Learning_group__c               == oldLAitem.Learning_group__c &&
                    newLAitem.redwing__Progress__c            == 'Completed') {

                    oldLAitem.redwing__Progress__c             = newLAitem.redwing__Progress__c;
                    oldLAitem.redwing__Progress_Percentage__c  = newLAitem.redwing__Progress_Percentage__c;
                    if (oldLAitem.redwing__Progress__c        == 'Completed') {
                        change = true;
                    }
                }
            }
        }
        if (change) {
            update listLA;
        }
    }

    private List<redwing__Learning_Assignment__c> getAllLAWithMultipleCompletion() {
        return [

                SELECT  Id,
                        Multiple_Completion__c,
                        redwing__Learning__c,
                        Learning_group__c,
                        redwing__Training_Plan__c,
                        redwing__Progress__c,
                        redwing__Progress_Percentage__c
                FROM redwing__Learning_Assignment__c
                WHERE Multiple_Completion__c = true
        ];
    }
}