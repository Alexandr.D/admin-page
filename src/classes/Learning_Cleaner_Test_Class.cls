/**
* Created by User on 13.09.2018.
*/

public with sharing class Learning_Cleaner_Test_Class {


    //public List<objectClass> customObjectList{ get; set; }
    @AuraEnabled
    public static Map<String, String> searchForIds() {
    List<Contact> listContact = [SELECT Id, Name FROM Contact LIMIT 10];
    Map<String, String> mapContacts = new Map<String, String>();
        for(Integer i=0; i<listContact.size(); i++) {
            mapContacts.put('a' + String.valueOf(i), listContact[i].Name);
        }

        System.debug('>>>' + listContact);
        return mapContacts;
    }
}