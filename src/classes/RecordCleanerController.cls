public class RecordCleanerController {

    private static Set<String> setOfObjectHierarchy = new Set<String>();
    private static Map<String, Schema.SObjectType> mapStringTypes = sortMap();
    private static Map<String, List<String>> objectsAndThemChildrenMap = getAllChildrenOfObjects();
    public static Map<Decimal, List<String>> mapOfParentsAndChildren = new Map<Decimal, List<String>>();

    private static Map<String, Schema.SObjectType> sortMap() {

        Map<String, Schema.SObjectType> customObjectsMap = new Map<String, SObjectType>();
        Map<String, Schema.SObjectType> mapGlobalDescribe = Schema.getGlobalDescribe();
        for (String item : mapGlobalDescribe.keySet()) {
            if ((item.containsIgnoreCase('learning') ||
                 item.containsIgnoreCase('achievement') ||
                 item.containsIgnoreCase('training')) &&
                 item.containsIgnoreCase('__c')) {
                 customObjectsMap.put(item, mapGlobalDescribe.get(item));
            }
        }
        return customObjectsMap;
    }

    @AuraEnabled
    public static Map<String, String> getAllCustomObjects() {

        Map<String, String> allCustomObjectsMap = new Map<String, String>();
        for (String item : mapStringTypes.keySet()) {
             String incomingItem = item.substringBefore('__');
             String shortItem = item.substringBeforeLast('__c').substringAfter('__');
             List<String> elements = shortItem.split('_');
             String result = incomingItem + '__';
             for (String count : elements) {
                 result += count.substring(0, 1).toUpperCase() + count.substring(1, count.length()) + '_';
             }
             result += '_c';
             allCustomObjectsMap.put(result, mapStringTypes.get(item).getDescribe().getLabel());
        }
        return allCustomObjectsMap;
    }

    private static Map<String, List<String>> getAllChildrenOfObjects() {

        List<Schema.SObjectType> schemaList = new List<Schema.SObjectType>();
        Map<String, List<String>> objectsAndThemChildrenMap = new Map<String, List<String>>();

        for (String item : mapStringTypes.keySet()) {
             schemaList.add(mapStringTypes.get(item));
        }
        for (Schema.SObjectType item : schemaList) {
            List<Schema.ChildRelationship> result = item.getDescribe().getChildRelationships();
            List<String> children = new List<String>();
            for (Schema.ChildRelationship child : result) {
                if ((child.getChildSObject().getDescribe().getName().containsIgnoreCase('learning') ||
                     child.getChildSObject().getDescribe().getName().containsIgnoreCase('achievement') ||
                     child.getChildSObject().getDescribe().getName().containsIgnoreCase('training')) &&
                     child.getChildSObject().getDescribe().getName().containsIgnoreCase('__c')) {
                     children.add(child.getChildSObject().getDescribe().getName());
                }
            }
            objectsAndThemChildrenMap.put(item.getDescribe().getName(), children);
        }
        return objectsAndThemChildrenMap;
    }

    @AuraEnabled
    public static Map<Decimal, List<String>> getChildrenOfSelectedObject(List<String> incomingObjects, Decimal level) {

        try {
            for (String item : incomingObjects) {
                if (!setOfObjectHierarchy.contains(item)) {
                    setOfObjectHierarchy.add(item);
                    if (mapOfParentsAndChildren.get(level) == null) {
                        mapOfParentsAndChildren.put(level, new List<String>());
                    }
                    mapOfParentsAndChildren.get(level).add(item);
                    List<String> children = objectsAndThemChildrenMap.get(item);
                    if (children != null) {
                        getChildrenOfSelectedObject(children, level + 1);
                    }
                }
            }
        } catch (NullPointerException e) {
            incomingObjects = null;
            level = null;
            ApexPages.Message messageException = new ApexPages.Message(ApexPages.Severity.error, e.getMessage());
            ApexPages.addMessage(messageException);
        }
        return mapOfParentsAndChildren;
    }

    @AuraEnabled
    public static Map<Decimal, List<String>> getParentsOfSelectedObject(List<String> listIncomingObjects) {

        Map<String, Schema.SObjectType> mapStringTypes = Schema.getGlobalDescribe();
        List<String> parentObjects = new List<String>();

        try {
            for (String item : listIncomingObjects) {
                Schema.SObjectType sObjType = mapStringTypes.get(item);
                Schema.DescribeSObjectResult resultP = sobjType.getDescribe().SObjectType.getDescribe();
                for (Schema.SobjectField parent : resultP.SObjectType.getDescribe().fields.getMap().Values()) {
                    if (parent.getDescribe().getType() == Schema.DisplayType.REFERENCE) {
                        if ((String.valueOf(parent.getDescribe().getReferenceTo()).containsIgnoreCase('learning') ||
                             String.valueOf(parent.getDescribe().getReferenceTo()).containsIgnoreCase('achievement') ||
                             String.valueOf(parent.getDescribe().getReferenceTo()).containsIgnoreCase('training')) &&
                             String.valueOf(parent.getDescribe().getReferenceTo()).containsIgnoreCase('__c')) {
                             parentObjects.add(String.valueOf(parent.getDescribe().getReferenceTo()).replace('(', '').replace(')', ''));
                        }
                    }
                }
            }
        } catch (NullPointerException e) {
            listIncomingObjects = null;
            mapStringTypes = null;
            ApexPages.Message messageException = new ApexPages.Message(ApexPages.Severity.error, e.getMessage());
            ApexPages.addMessage(messageException);
        }
        mapOfParentsAndChildren.put(0, parentObjects);
        return mapOfParentsAndChildren;
    }

    @AuraEnabled
    public static void deleteRecordsOfObjects(List<String> incomingObjects, Boolean checkbox) {

        Map<Decimal, List<String>> learningObjectMap = new Map<Decimal, List<String>>();
        List<String> learningObject = new List<String>{'redwing__Learning__c'};
        Boolean check = false;
        try {
            if (checkbox) {
                getChildrenOfSelectedObject(learningObject, 1);
                getParentsOfSelectedObject(learningObject);
                learningObjectMap.putAll(mapOfParentsAndChildren);
                mapOfParentsAndChildren.clear();
                setOfObjectHierarchy.clear();
            }
            getChildrenOfSelectedObject(incomingObjects, 1);
            getParentsOfSelectedObject(incomingObjects);
            if (checkbox) {
                for (Decimal item : mapOfParentsAndChildren.keySet()) {
                    for (String item2 : learningObjectMap.get(item)) {
                        if (mapOfParentsAndChildren.get(item).contains(item2)) {
                            check = true;
                        }
                    }
                }
            }
            if (check) {
                mapOfParentsAndChildren.put(0, learningObject);
            }
        } catch (NullPointerException e) {
            checkbox = null;
            incomingObjects = null;
            ApexPages.Message messageException = new ApexPages.Message(ApexPages.Severity.error, e.getMessage());
            ApexPages.addMessage(messageException);
        }
        
        if (!mapOfParentsAndChildren.isEmpty()) {
            DataBase.executeBatch(new DeleteAllRecordsBATCH(mapOfParentsAndChildren));
        }
    }
}