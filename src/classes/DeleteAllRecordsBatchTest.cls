/**
 * Created by User on 28.09.2018.
 */
@isTest

public class DeleteAllRecordsBatchTest {
    @TestSetup
    static void setup() {

        redwing__Training_Plan__c testPlan = new redwing__Training_Plan__c(Name = 'TestPlan');
        redwing__Learning__c testLearning = new redwing__Learning__c(Name = 'TestLearning');
        redwing__Achievement__c testAchievement = new redwing__Achievement__c(Name = 'testAchievement');
        redwing__Training_Activity__c testActivity = new redwing__Training_Activity__c(redwing__Achievement__c = testAchievement.Id,
                                                                                       redwing__Learning__c = testLearning.Id);
        insert testPlan;
        insert testLearning;
        insert testAchievement;
        insert testActivity;
    }

    static testmethod void test() {
        Map<Decimal,List<String>> currentObjectLevels = new Map<Decimal,List<String>>();
        List<String>tstLst_1 = new List<String>();
        tstLst_1.add('redwing__Training_Plan__c');
        tstLst_1.add('redwing__Training_Plan_Section__c');
        currentObjectLevels.put(0,tstLst_1);

        Test.startTest();
        DeleteAllRecordsBATCH testBatch = new DeleteAllRecordsBATCH(currentObjectLevels);
        DataBase.executeBatch(testBatch);
        Test.stopTest();

        System.assertEquals(0, [SELECT count() FROM redwing__Training_Plan__c]);
        System.assertEquals(0, [SELECT count() FROM redwing__Training_Plan_Section__c]);

    }

    static testMethod void testException() {
        Map<Decimal,List<String>> currentObjectLevels = new Map<Decimal,List<String>>();
        List<String>firstTestListStrings = new List<String>();
        List<String>secondTestListStrings = new List<String>();
        firstTestListStrings.add('redwing__Training_Plan__c');
        secondTestListStrings.add('redwing__Training_Plan_Section__c');
        currentObjectLevels.put(0,firstTestListStrings);
        currentObjectLevels.put(1,secondTestListStrings);
        Test.startTest();
        DeleteAllRecordsBATCH testBatch = new DeleteAllRecordsBATCH(currentObjectLevels);
        DataBase.executeBatch(testBatch);
        Test.stopTest();

        System.assertEquals(0, [SELECT count() FROM redwing__Training_Plan__c]);
        System.assertEquals(0, [SELECT count() FROM redwing__Training_Plan_Section__c]);

    }
}