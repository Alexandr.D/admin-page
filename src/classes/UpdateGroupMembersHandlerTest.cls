@isTest
public with sharing class UpdateGroupMembersHandlerTest {

    @testSetup static void setup() {
        // Create common test accounts
        List<redwing__Learning_Assignment__c> listLearningAssignments = new List<redwing__Learning_Assignment__c>();
        for(Integer i=0;i<2;i++) {
            listLearningAssignments.add(new redwing__Learning_Assignment__c(redwing__Progress__c = 'Not Started',Multiple_Completion__c = true));
        }
        insert listLearningAssignments;
    }

    @isTest
    public static void assignCompletedTaskTest(){
        List<redwing__Learning_Assignment__c> listLearningAssignments = [SELECT Id,
                                                                                Multiple_Completion__c,
                                                                                redwing__Progress__c
                                                                                FROM redwing__Learning_Assignment__c LIMIT 1];
        listLearningAssignments[0].redwing__Progress__c = 'Completed';
        UpdateGroupMembersHandler ugmh = new UpdateGroupMembersHandler();
        ugmh.assignCompletedTask(listLearningAssignments);
        List<redwing__Learning_Assignment__c> listLearningAssignments2 = [SELECT Id,
                                                                                Multiple_Completion__c,
                                                                                redwing__Progress__c
                                                                         FROM redwing__Learning_Assignment__c
                                                                         WHERE Multiple_Completion__c = true
                                                                         LIMIT 1];
        System.assertEquals(listLearningAssignments2[0].redwing__Progress__c, 'Completed');
    }
}