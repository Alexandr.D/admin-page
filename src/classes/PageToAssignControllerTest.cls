/**
 * Created by User on 09.10.2018.
 */
@isTest
public with sharing class PageToAssignControllerTest {
    @TestSetup
    public static void setup() {
        Profile testProfile = [SELECT Id
                               FROM Profile
                               WHERE Name='Standard User'];
        List<User> listTestUsers = new List<User>();
        User firstTestUser = new User(Alias = 'standt',
                                      Email='standarduser123321@testorg.com',
                                      EmailEncodingKey='UTF-8',
                                      LastName='Testing',
                                      LanguageLocaleKey='en_US',
                                      LocaleSidKey='en_US',
                                      ProfileId = testProfile.Id,
                                      TimeZoneSidKey='America/Los_Angeles',
                                      UserName='123321standarduser123321ZHOPABIG@testorg.com',
                                      Active_mentor__c = false );

        User secondTestUser = new User(Alias = 'stndt',
                                       Email='stanrduser123321@testorg.com',
                                       EmailEncodingKey='UTF-8',
                                       LastName='Testin',
                                       LanguageLocaleKey='en_US',
                                       LocaleSidKey='en_US',
                                       ProfileId = testProfile.Id,
                                       TimeZoneSidKey='America/Los_Angeles',
                                       UserName='andarduser123321ZHOPABIG@testorg.com',
                                       Active_mentor__c = true );
        listTestUsers.add(firstTestUser);
        listTestUsers.add(secondTestUser);
        insert listTestUsers;

        CollaborationGroup testCollaborationGroup = new CollaborationGroup(Name = 'MyCollaborationGroup',
                                                                           CollaborationType = 'unlisted');
        insert testCollaborationGroup;

        CollaborationGroupMember testGroup = new CollaborationGroupMember(CollaborationGroupId = testCollaborationGroup.id,
                                                                          MemberId = firstTestUser.id);
        insert testGroup;

        List<redwing__Training_Track__c> listTracs = new List<redwing__Training_Track__c>{
            new redwing__Training_Track__c(Name = 'First week'),
            new redwing__Training_Track__c(Name = 'Second week'),
            new redwing__Training_Track__c(Name = 'Third week')
        };
        insert listTracs;

        List<Organization__c> listOrganizationTest = new List<Organization__c> {
            new Organization__c(Name = 'Org1'),
            new Organization__c(Name = 'Org2'),
            new Organization__c(Name = 'Org3')
        };
        insert listOrganizationTest;

        Junction_Object__c testJunctionObject = new Junction_Object__c(ChildOrganization__c = listOrganizationTest[2].Id, FromJunctionObject__c = secondTestUser.Id);
        insert testJunctionObject;

    }

    @isTest
    public static void getCohortNameTest() {

        PageToAssignController TestPageToAssignController = new PageToAssignController();
        Test.startTest();

        List<SelectOption> testSelectOptions = TestPageToAssignController.getCohortName();

        Test.stopTest();

        System.assertEquals(testSelectOptions.get(2).getValue(), 'September');
        System.assertEquals(testSelectOptions.get(1).getValue(), 'October');
    }

    @isTest
    public static void getOrganizationsNameTest() {
        List<Organization__c> listOrganizationTest = [SELECT Name
                                                      FROM Organization__c
                                                      LIMIT 3];

        PageToAssignController TestPageToAssignController = new PageToAssignController();
        Test.startTest();

        List<SelectOption> testSelectOptions = TestPageToAssignController.getOrganizationsName();

        Test.stopTest();

        //System.assertEquals(testSelectOptions.get(3).getValue(), 'We Like VRP');
        System.assertEquals(testSelectOptions.get(1).getValue(), 'Org1');
    }

    @isTest
    public static void getSegroupsNameTest() {

        List<User> testUsers = [SELECT ID, Name
                                FROM User
                                WHERE Profile.Name = 'Standard user'
                                LIMIT 2];

        List<String> testListIds = new List<String>();
        for(User testUserInLoop : testUsers) {
            testListIds.add(testUserInLoop.Id);
        }

        PageToAssignController TestPageToAssignController = new PageToAssignController();
        TestPageToAssignController.allUserSetIds = testListIds;

        CollaborationGroup testCollaborationGroup = new CollaborationGroup(Name = 'MyCollaborationGroup',
                                                                           CollaborationType = 'unlisted');
        insert testCollaborationGroup;

        CollaborationGroupMember testGroup = new CollaborationGroupMember(CollaborationGroupId = testCollaborationGroup.id,
                                                                          MemberId = testUsers[0].id);
        insert testGroup;

        Test.startTest();
        List<SelectOption> testSelectOptions = TestPageToAssignController.getSegroupsName();
        Test.stopTest();

        System.assertNotEquals(null ,testSelectOptions);
    }

    @isTest
    public static void getTracksNameTest() {

        List<redwing__Training_Track__c> listTracs = [SELECT Name
                                                      FROM redwing__Training_Track__c
                                                      LIMIT 3];
        PageToAssignController TestPageToAssignController = new PageToAssignController();
        Test.startTest();

        List<SelectOption> testSelectOptions = TestPageToAssignController.getTracksName();

        Test.stopTest();

        System.assertEquals(testSelectOptions.get(2).getLabel(), 'Second week');
        System.assertEquals(testSelectOptions.get(1).getLabel(), 'First week');
    }

    @isTest
    public static void UsersFromCohortTest() {
        List<User> listTestUsers = [SELECT ID, Name
                                    FROM User
                                    WHERE Profile.Name = 'Standard user'
                                    LIMIT 2];

        List<Group> listTestGroups = new List<Group>{
                new Group(Name = 'First week'),
                new Group(Name = 'Second week')
        };
        insert listTestGroups;

        GroupMember testGroupMember = new GroupMember(UserOrGroupId = listTestUsers[1].Id,
                GroupId = listTestGroups[0].Id);
        insert testGroupMember;

        List<Organization__c> listTestOrganizations = [SELECT Name
                                                       FROM Organization__c];

        PageToAssignController TestPageToAssignController = new PageToAssignController();
        TestPageToAssignController.incomingCohort = listTestGroups[1].Name;
        TestPageToAssignController.incomingOrganization = null;

        Test.startTest();
        TestPageToAssignController.UsersFromCohort();
        Test.stopTest();

        System.assertEquals(0, TestPageToAssignController.allUsersSet.size());

    }
    @isTest
    public static void UsersFromCohort2Test() {
        PageToAssignController TestPageToAssignController = new PageToAssignController();
        TestPageToAssignController.incomingCohort = 'September';
        TestPageToAssignController.incomingOrganization = 'Org1';
        TestPageToAssignController.UsersFromCohort();
    }

    @isTest
    public static void UsersFromSeGroupTest() {
        PageToAssignController TestPageToAssignController = new PageToAssignController();
        List<User> listUsers = [SELECT Name, Active_mentor__c
                                FROM User];
        TestPageToAssignController.incomingSeGroup = 'Org1';
        TestPageToAssignController.UsersFromSeGroup();
    }
    @isTest
    public static void AddUsersToAssignTest() {
        PageToAssignController TestPageToAssignController = new PageToAssignController();
        User u = [SELECT Id, Name FROM User LIMIT 1];
        List<User> listUsers = [SELECT Name,Active_mentor__c FROM User];
        Test.StartTest();
        PageReference pageRef = Page.PageToAssign;
        pageRef.getParameters().put('IdAdd', u.Id);
        Test.setCurrentPage(pageRef);
        TestPageToAssignController.allUsersSet = listUsers;
        TestPageToAssignController.AddUsersToAssign();
        Test.StopTest();
    }
    @isTest
    public static void changeTrackTest() {
        PageToAssignController TestPageToAssignController = new PageToAssignController();
        Test.StartTest();
        PageReference pageRef = Page.PageToAssign;
        pageRef.getParameters().put('valueTrack', 'TestTrack');
        Test.setCurrentPage(pageRef);
        TestPageToAssignController.changeTrack();
        Test.StopTest();
    }


    @isTest
    public static void RemoveUsersToAssignTest() {
        PageToAssignController TestPageToAssignController = new PageToAssignController();
        User u = [SELECT Id, Name FROM User LIMIT 1];
        List<User> listUsers = [SELECT Name FROM User];
        Test.StartTest();
        PageReference pageRef = Page.PageToAssign;
        pageRef.getParameters().put('IdRemove', u.Id);
        Test.setCurrentPage(pageRef);
        TestPageToAssignController.listUsersToAssign = listUsers;
        TestPageToAssignController.RemoveUsersToAssign();
        Test.StopTest();
    }

    @isTest
    public static void AddMentorForUserToAssignTest() {
        PageToAssignController TestPageToAssignController = new PageToAssignController();
        User testUser = [SELECT Id, Name FROM User LIMIT 1];
        List<User> listUsers = [SELECT Name, Active_mentor__c FROM User];
        Test.StartTest();
        PageReference pageRef = Page.PageToAssign;
        pageRef.getParameters().put('IdMentor', testUser.Id);
        Test.setCurrentPage(pageRef);
        TestPageToAssignController.listUsersToAssign = listUsers;
        TestPageToAssignController.AddMentorForUserToAssign();
        Test.StopTest();

    }

    @isTest
    public static void ClearListToAssignTest() {
        PageToAssignController TestPageToAssignController = new PageToAssignController();
        TestPageToAssignController.ClearListToAssign();
    }
    @isTest
    public static void assignTrackTest() {
        PageToAssignController TestPageToAssignController = new PageToAssignController();
        TestPageToAssignController.assignTrack();
    }


    private static void TestData() {
        List<Organization__c> listOrganizationTest = new List<Organization__c> {
                new Organization__c(Name = 'Org1'),
                new Organization__c(Name = 'Org2'),
                new Organization__c(Name = 'Org3')
        };
        insert listOrganizationTest;

    }

}