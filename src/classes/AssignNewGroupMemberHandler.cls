public class AssignNewGroupMemberHandler {

    private List<redwing__Learning_Assignment__c> listLA;
    private Map<Id, Boolean> mapUsersIds;
    private Map<Id, String> mapGroups;

    public AssignNewGroupMemberHandler() {
        this.listLA = getAllLAWithMultipleCompletion();
        this.mapGroups = getAllGroups();
        this.mapUsersIds = getAllUsers();
    }

    public void assignNewMember(List<CollaborationGroupMember> newMember) {
        List<redwing__Learning_Assignment__c> updateListChangesOfLA = new List<redwing__Learning_Assignment__c>();
        try {
            if (newMember != null && listLA != null) {
                for (CollaborationGroupMember memberItem : newMember) {
                    for (redwing__Learning_Assignment__c itemLA : listLA) {
                        if (itemLA.redwing__User__c == memberItem.MemberId &&
                            mapGroups.keySet().contains(memberItem.CollaborationGroupId)) {

                            itemLA.Learning_group__c = mapGroups.get(memberItem.CollaborationGroupId);
                            updateListChangesOfLA.add(itemLA);
                        }
                        if (mapUsersIds.keySet().contains(memberItem.MemberId) &&
                            mapUsersIds.get(memberItem.MemberId)     == true &&
                            mapGroups.keySet().contains(memberItem.CollaborationGroupId)) {

                            if (itemLA.Mentor_1_look__c              == null &&
                                itemLA.Mentor_2_look__c              != memberItem.MemberId &&
                                itemLA.Mentor_3_look__c              != memberItem.MemberId &&
                                itemLA.Multiple_Completion__c        == true &&
                                itemLA.Learning_group__c             == mapGroups.get(memberItem.CollaborationGroupId)) {
                                
                                itemLA.Mentor_1_look__c               = memberItem.MemberId;
                            } else if (itemLA.Mentor_2_look__c       == null &&
                                       itemLA.Mentor_1_look__c       != memberItem.MemberId &&
                                       itemLA.Mentor_3_look__c       != memberItem.MemberId &&
                                       itemLA.Multiple_Completion__c == true &&
                                       itemLA.Learning_group__c      == mapGroups.get(memberItem.CollaborationGroupId)) {

                                       itemLA.Mentor_2_look__c        = memberItem.MemberId;
                            } else if (itemLA.Mentor_3_look__c       == null &&
                                       itemLA.Mentor_1_look__c       != memberItem.MemberId &&
                                       itemLA.Mentor_2_look__c       != memberItem.MemberId &&
                                       itemLA.Multiple_Completion__c == true &&
                                       itemLA.Learning_group__c      == mapGroups.get(memberItem.CollaborationGroupId)) {

                                       itemLA.Mentor_3_look__c        = memberItem.MemberId;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.debug('Error: ' + e.getMessage());
        }
        update listLA;
    }

    public void deleteLearningGroup(List<CollaborationGroupMember> deletedMember) {
        List<redwing__Learning_Assignment__c> updateListChangesOfLA = new List<redwing__Learning_Assignment__c>();
        Set<Id> userNames = new Set<Id>();
        for (CollaborationGroupMember itemMember : deletedMember) {
            for (redwing__Learning_Assignment__c itemLA : listLA) {
                if (itemLA.redwing__User__c == itemMember.MemberId) {
                    itemLA.Learning_group__c = '';
                }
                if (itemLA.Multiple_Completion__c           = true &&
                    itemLA.redwing__Progress__c            != 'Not Started' &&
                    itemLA.redwing__Progress_Percentage__c != 0.00) {

                    itemLA.redwing__Progress__c             = 'Not Started';
                    itemLA.redwing__Progress_Percentage__c  = 0.00;
                }
                if (mapUsersIds.keySet().contains(itemMember.MemberId) &&
                    mapUsersIds.get(itemMember.MemberId)   == true) {

                    userNames.add(itemMember.MemberId);
                }
            }
        }

        for (CollaborationGroupMember itemMember : deletedMember) {
            for (redwing__Learning_Assignment__c itemLA : listLA) {
                if (mapGroups.keySet().contains(itemMember.CollaborationGroupId)) {
                    if (userNames.contains(itemLA.Mentor_1_look__c)) {
                        itemLA.Mentor_1_look__c = null;

                    } else if (userNames.contains(itemLA.Mentor_2_look__c)) {
                        itemLA.Mentor_2_look__c = null;

                    } else if (userNames.contains(itemLA.Mentor_3_look__c)) {
                        itemLA.Mentor_3_look__c = null;
                    }
                }
            }
        }
        update listLA;
    }

    private List<redwing__Learning_Assignment__c> getAllLAWithMultipleCompletion() {
        return [

                SELECT  Id,
                        redwing__User__c,
                        Multiple_Completion__c,
                        Learning_group__c,
                        redwing__Training_Plan__c,
                        redwing__Progress__c,
                        redwing__Progress_Percentage__c,
                        Mentor_1_look__c,
                        Mentor_2_look__c,
                        Mentor_3_look__c
                FROM redwing__Learning_Assignment__c
        ];
    }

    private Map<Id, String> getAllGroups() {

        Map<Id, String> mapAllGroups = new Map<Id, String>();
        List<CollaborationGroup> listGroups = [

                SELECT Id, Name
                FROM CollaborationGroup

        ];

        for (CollaborationGroup item : listGroups) {
            mapAllGroups.put(item.Id, item.Name);
        }
        return mapAllGroups;
    }

    private Map<Id, Boolean> getAllUsers() {

        Map<Id, Boolean> mapUsersIds = new Map<Id, Boolean>();
        List<User> listUsers = [

                SELECT Id, Active_mentor__c
                FROM User
        ];

        for (User item : listUsers) {
            mapUsersIds.put(item.Id, item.Active_mentor__c);
        }
        return mapUsersIds;
    }
}