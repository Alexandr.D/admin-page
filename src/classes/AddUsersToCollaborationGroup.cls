/**
 * Created by User on 25.09.2018.
 */

public with sharing class AddUsersToCollaborationGroup {

    public AddUsersToCollaborationGroup() {
        List<User> viewUsers = new List<User>();
        viewUsers = [SELECT Id, Name FROM User LIMIT 100];
    }

    public List<User> userToView() {
        List<User> viewUsers2 = new List<User>();
        viewUsers2 = [SELECT Id, Name FROM User LIMIT 100];

        return viewUsers2;
    }

}