/**
 * Created by User on 25.09.2018.
 */

global class DeleteAllRecordsBATCH implements Database.Batchable<sObject> {
    private Map<Decimal, List<String>> mapOfObjectsToDeleteBatch = new Map<Decimal, List<String>>();
    private List<SObject> recordsToDelete = new List<SObject>();
    private String lastApiNameInMapBatch;
    private Integer levelOfMap;

    global DeleteAllRecordsBATCH(Map<Decimal, List<String>> mapObjectsToDelete){

        this.mapOfObjectsToDeleteBatch = mapObjectsToDelete;
        Integer levelMapNow = mapOfObjectsToDeleteBatch.size()-1;

        //get value of last API Name in last row
        this.lastApiNameInMapBatch = mapOfObjectsToDeleteBatch.get(levelMapNow).get(mapOfObjectsToDeleteBatch.get(levelMapNow).size()-1);

    }

    global Database.QueryLocator start(Database.BatchableContext BC){

        String query = 'SELECT Id FROM ' + lastApiNameInMapBatch + ' ';
        return  Database.getQueryLocator(query);

    }

    global  void execute(Database.BatchableContext BC, List<SObject> listToDelete){
        if(listToDelete.size() != null){
            try {
                delete listToDelete;
            } catch (DmlException e) {
                System.debug('DML exception has occurred ' + e.getMessage());
            }
        }
    }

    global void finish(Database.BatchableContext BC){

        //Map for new Batch
        this.levelOfMap = mapOfObjectsToDeleteBatch.size()-1;
        mapOfObjectsToDeleteBatch.get(levelOfMap).remove(mapOfObjectsToDeleteBatch.get(levelOfMap).size()-1);

        //delete last row if value is null
        if(mapOfObjectsToDeleteBatch.get(mapOfObjectsToDeleteBatch.size() - 1).isEmpty()) {
            mapOfObjectsToDeleteBatch.remove(mapOfObjectsToDeleteBatch.size() - 1);
        }

        if(!mapOfObjectsToDeleteBatch.isEmpty()) {
            try{
                DataBase.executeBatch(new DeleteAllRecordsBATCH(mapOfObjectsToDeleteBatch));
            } catch (Exception otherException){
                System.debug ('Other exception in Batch' + otherException.getMessage());
            }
        }
    }
}