@isTest
public with sharing class DeleteJunctionObjectHandlerTest {

    @testSetup static void setup() {
        CollaborationGroup grp = new CollaborationGroup();
        grp.name = 'TestGroup';
        grp.CollaborationType = 'Unlisted';
        Insert grp;

        User usr = TestUser();
        List<CollaborationGroupMember> grpMem1 = new List<CollaborationGroupMember>();
        grpMem1.add(new CollaborationGroupMember(MemberId = usr.Id, CollaborationGroupId = grp.Id));
        Insert grpMem1;

        List<Organization__c> listOrganizations = new List<Organization__c>();
        listOrganizations.add(new Organization__c(Name = 'Test Organization'));
        insert listOrganizations;

        List<redwing__Learning_Assignment__c> listLearningAssignments = new List<redwing__Learning_Assignment__c>();
        listLearningAssignments.add(new redwing__Learning_Assignment__c(redwing__Progress__c = 'Completed',Multiple_Completion__c = true,Learning_group__c = '',
                 redwing__User__c = usr.Id, redwing__Progress_Percentage__c = 1.10));
        insert listLearningAssignments;



        List<Junction_Object__c> listJunctionObjects = new List<Junction_Object__c>();
        listJunctionObjects.add(new Junction_Object__c(ChildOrganization__c = listOrganizations[0].id,	FromJunctionObject__c = usr.id,	Name = 'Test Junction'));
        listJunctionObjects.add(new Junction_Object__c(ChildOrganization__c = listOrganizations[0].id,	FromJunctionObject__c = usr.id,	Name = 'Test Junction2'));
        insert listJunctionObjects;
    }

    @isTest
    public static void deleteJunctionTest(){
        List<Junction_Object__c> listJunctionObject = [SELECT Id,FromJunctionObject__c FROM Junction_Object__c];
        try{
            delete listJunctionObject;
        } catch (DmlException e) {
        DeleteJunctionObjectHandler djoh = new DeleteJunctionObjectHandler();
        djoh.deleteJunction(listJunctionObject);
        List<redwing__Learning_Assignment__c> listLearningAssignments = [SELECT redwing__Progress__c FROM redwing__Learning_Assignment__c];
        System.assertEquals(listLearningAssignments[0].redwing__Progress__c, 'Not Started');
        }
    }

    private static User TestUser() {
        Profile p = [select Id, Name from Profile where Name = 'Standard User'];
        User u = new User(
                UserName = 'tst-user@test-company.com',
                FirstName = 'Tst-First-Name',
                LastName = 'Test-Last-Name',
                Alias = 'test',
                Email = 'tesst-user@test-company.com',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocalesIdKey = 'en_US',
                TimezonesIdKey = 'America/Los_Angeles',
                ProfileId = p.Id,
                Active_mentor__c = true
        );
        insert u;
        return u;
    }
}