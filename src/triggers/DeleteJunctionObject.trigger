trigger DeleteJunctionObject on Junction_Object__c (before delete) {
    if (Trigger.isDelete) {
        DeleteJunctionObjectHandler instance = new DeleteJunctionObjectHandler();
        instance.deleteJunction(Trigger.new);
    }
}