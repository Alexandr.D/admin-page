trigger UpdateGroupMembers on redwing__Learning_Assignment__c (after update) {

    if (Trigger.isUpdate) {
        UpdateGroupMembersHandler instance = new UpdateGroupMembersHandler();
        instance.assignCompletedTask(Trigger.new);
    }
}