trigger AssignNewGroupMember on CollaborationGroupMember (after insert, before delete) {

    if (Trigger.isInsert) {
        AssignNewGroupMemberHandler instance = new AssignNewGroupMemberHandler();
        instance.assignNewMember(Trigger.new);
    }

    if (Trigger.isDelete) {
        AssignNewGroupMemberHandler instance = new AssignNewGroupMemberHandler();
        instance.deleteLearningGroup(Trigger.old);
    }
}